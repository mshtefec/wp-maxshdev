<?php
use FileBird\Model\Folder as FolderModel;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
function filebird_gallery_fb_block_assets() {
	wp_register_script(
		'filebird_gallery-fb-block-js',
		plugins_url( '/dist/blocks.build.js', dirname( __FILE__ ) ),
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ),
		null,
		true
	);

	wp_localize_script(
		'filebird_gallery-fb-block-js',
		'fbGlobal',
		[
			'pluginDirPath' => plugin_dir_path( __DIR__ ),
			'pluginDirUrl'  => plugin_dir_url( __DIR__ ),
		]
	);

	register_block_type(
		'filebird/block-filebird-gallery', array(
			'editor_script' => 'filebird_gallery-fb-block-js',
			'render_callback' => 'filebird_gallery_render',	
			'attributes' => array(
				'selectedFolder' => array(
					'type' => 'array',
					'default' => array()
				),
				'captions' => array(
					'type' => 'object',
					'default' => array()
				),
				'imagesRemoved' => array(
					'type' => 'array',
					'default' => array()
				),
                'images' => array(
					'type' => 'array',
					'default' => array()
				),
				'columns' => array(
					'type' => 'integer',
					'default' => 3
				),
				'isCropped' => array(
					'type' => 'boolean',
					'default' => true
				),
				'linkTo' => array(
					'type' => 'string',
					'default' => 'none'
				),
				'sortBy' => array(
					'type' => 'string',
					'default' => 'date'
				),
				'sortType' => array(
					'type' => 'string',
					'default' => 'DESC'
				)
            )
		)
	);
}

function filebird_gallery_render( $attributes ){
	global $wpdb;

	if (empty($attributes['selectedFolder'])) return '';
	
    $where_arr = array('1 = 1');
	$where_arr[] = "`folder_id` IN (".implode(',', $attributes['selectedFolder']).")";
	$in_not_in = $wpdb->get_col("SELECT `attachment_id` FROM {$wpdb->prefix}fbv_attachment_folder". " WHERE " . implode(' AND ', apply_filters('fbv_in_not_in_where_query', $where_arr, $attributes['selectedFolder'])));
	// $in_not_in = count($attributes['selectedFolder']) > 0 ? FolderModel::getInAndNotInIds($attributes['selectedFolder']) : array();

	if (empty($in_not_in)) return '';

	$args = array(
        'post_type' => 'attachment',
		'posts_per_page' => -1,
		'fields' => 'ids',
		'post__in' => array_diff($in_not_in, $attributes['imagesRemoved']),
		'orderby' => $attributes['sortBy'],
		'order' => $attributes['sortType'],
	);
	$imageIds = get_posts($args);
	$ulClass = 'wp-block-filebird-block-filebird-gallery wp-block-gallery blocks-gallery-grid';
	$ulClass .= ' columns-' . $attributes['columns'];
	$ulClass .= $attributes['isCropped'] ? ' is-cropped' : '';

	if (count($imageIds) < 1) return '';

	$html = '';
	$html .= '<ul class="' . $ulClass . '">';

	foreach ($imageIds as $key => $imgId) {
		$href = '';
		$url = wp_get_attachment_url($imgId);
		switch ($attributes['linkTo']) {
			case 'media':
				$href = wp_get_attachment_url($imgId);
				break;
			case 'attachment':
				$href = $image['link'];
				break;
			default:
				break;
		}

		$img = '<img src="' . $url . '"' . ' alt="' . 'img' . '"';   
		$img .= 'class="' . "wp-image-{$imgId}" . "/>";

		$li = '<li class="blocks-gallery-item">';
		$li .= '<figure>';
		
		$li .= empty($href) ? $img : '<a href="' . $href . '">' . $img . '</a>';
		$li .= empty($attributes['captions'][$imgId]) ? '' : '<figcaption class="blocks-gallery-item__caption">' . $attributes['captions'][$imgId] . '</figcaption>';
		$li .= '</figure>';
		$li .= '</li>';

		$html .= $li;
	}

	$html .= '</ul>';

	return $html;
}

add_action( 'init', 'filebird_gallery_fb_block_assets' );
